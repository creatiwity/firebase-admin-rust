// Common modules
pub mod app;
pub mod auth;
pub mod credentials;
pub mod messaging;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
