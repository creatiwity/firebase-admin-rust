use super::error::{AuthError, KeyError};
use crate::app::App;
use ber::BerObjectContent;
use chrono::Utc;
use der::{parse_der_integer, DerObject};
use der_parser::*;
use error::BerResult;
use jsonwebtoken::{
    decode, decode_header, encode, Algorithm, DecodingKey, EncodingKey, Header, Validation,
};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use url::Url;

const ID_TOKEN_CERT_URI: &'static str =
    "https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com";
const FIREBASE_AUDIENCE: &'static str =
    "https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit";
const ONE_HOUR_IN_SECONDS: i64 = 60 * 60;

#[derive(Hash, Eq, PartialEq, Serialize, Deserialize, Debug)]
pub enum SignInProvider {
    #[serde(rename = "google.com")]
    Google,
    #[serde(rename = "facebook.com")]
    Facebook,
    #[serde(rename = "firebase")]
    Firebase,
    #[serde(rename = "github.com")]
    Github,
    #[serde(rename = "phone")]
    Phone,
    #[serde(rename = "playgames.google.com")]
    PlayGames,
    #[serde(rename = "twitter.com")]
    Twitter,
    #[serde(rename = "email")]
    Email,
    #[serde(rename = "password")]
    Password,
}

#[serde(default)]
#[derive(Default, Serialize, Deserialize, Debug)]
pub struct FirebaseClaims {
    pub identities: HashMap<SignInProvider, Vec<String>>,
    pub sign_in_provider: Option<SignInProvider>,
}

#[serde(default)]
#[derive(Default, Serialize, Deserialize, Debug)]
pub struct DecodedIdToken {
    pub picture: Option<Url>,
    iss: Option<Url>,
    aud: String,
    auth_time: i64,
    user_id: String,
    pub sub: String,
    iat: i64,
    exp: i64,
    pub email: Option<String>,
    pub email_verified: bool,
    pub firebase: FirebaseClaims,
}

/// Our claims struct, it needs to derive `Serialize` and/or `Deserialize`
#[derive(Debug, Serialize, Deserialize)]
struct CustomClaims {
    aud: String,
    iat: i64,
    exp: i64,
    iss: String,
    sub: String,
    uid: String,
}

/*
 * Test for a the sequence that contains the key (ie: modulus + exponent)
 */
fn rsa_key_parser(seq: &[u8]) -> BerResult {
    parse_der_sequence_defined!(seq, parse_der_integer >> parse_der_integer)
}

/*
 * The DER-encoded public key is stored within a bitstring embedded either
 * in a certificate or an x.509 public key.
 *
 * To find the bitstring we need to sift through the DER field sequences
 * recursively.
 */
fn rec_extract_key(sequence: &Vec<DerObject>) -> Option<Vec<u8>> {
    for o in sequence {
        match &o.content {
            BerObjectContent::BitString(_, bso) => {
                match rsa_key_parser(bso.data) {
                    Ok(_) => {
                        let mut key: Vec<u8> = Vec::new();
                        key.extend_from_slice(bso.data);

                        return Some(key);
                    }
                    Err(_) => (),
                };
            }
            BerObjectContent::OctetString(oso) => {
                let mut key: Vec<u8> = Vec::new();
                key.extend_from_slice(oso);

                return Some(key);
            }
            BerObjectContent::Sequence(s) => match rec_extract_key(s) {
                Some(k) => return Some(k),
                None => (),
            },
            _ => (),
        }
    }

    return None;
}

/*
 * Extract a DER-formatted key from either a certificate or an x.509 public key
 */
fn extract_key(cert: &[u8]) -> Result<Vec<u8>, KeyError> {
    let result = parse_der(cert);
    let (_, der_object) = match result {
        Ok(p) => p,
        _ => return Err(KeyError::NotParsable),
    };

    match der_object.content {
        BerObjectContent::Sequence(s) => Ok(rec_extract_key(&s).ok_or(KeyError::KeyNotFound)?),
        _ => Err(KeyError::KeyNotFound),
    }
}

async fn public_certificates() -> Result<HashMap<String, String>, KeyError> {
    reqwest::get(ID_TOKEN_CERT_URI)
        .await?
        .json::<HashMap<String, String>>()
        .await
        .map_err(|err| err.into())
}

async fn cert_der(key_id: String) -> Result<Vec<u8>, KeyError> {
    let certs = public_certificates().await?;
    let cert = certs.get(&key_id).ok_or(KeyError::NotFound)?;
    let cert_der = pem::parse(cert)?;

    extract_key(cert_der.contents.as_slice())
}

fn key_der(private_key_pem: String) -> Result<Vec<u8>, KeyError> {
    let parsed_key = pem::parse(private_key_pem).map(|key| key.contents)?;
    extract_key(parsed_key.as_slice())
}

pub async fn decode_firebase_token(app: App, token: String) -> Result<DecodedIdToken, AuthError> {
    // Find public key id in token header
    let header =
        decode_header(&token).map_err(|error| AuthError::TokenDecodeError { jwt_error: error })?;

    let kid = header.kid.ok_or(AuthError::TokenHeaderMalformed)?;

    // Find corresponding public key in Google data
    let cert_der = cert_der(kid).await?;

    let mut validation = Validation::new(Algorithm::RS256);
    validation.set_audience(&[app.project_id.clone()]);
    validation.validate_exp = true;
    validation.iss = Some(format!("https://securetoken.google.com/{}", app.project_id));

    let decoded_token = decode::<DecodedIdToken>(
        token.as_str(),
        &DecodingKey::from_rsa_der(cert_der.as_slice()),
        &validation,
    )
    .map_err(|err| AuthError::TokenDecodeError { jwt_error: err })?;

    Ok(decoded_token.claims)
}

pub fn create_custom_token(app: App, uid: String) -> Result<String, AuthError> {
    // Prepare header
    let mut header = Header::default();
    header.alg = Algorithm::RS256;
    header.typ = Some(String::from("JWT"));

    // Prepare claims
    let now = Utc::now().timestamp();
    let claims = CustomClaims {
        aud: String::from(FIREBASE_AUDIENCE),
        iat: now,
        exp: now + ONE_HOUR_IN_SECONDS,
        iss: app.credential.client_email.clone(),
        sub: app.credential.client_email.clone(),
        uid,
    };

    // Prepare secret key
    let private_key = key_der(app.credential.private_key.clone())?;

    // Encode JWT
    encode(
        &header,
        &claims,
        &EncodingKey::from_rsa_der(private_key.as_slice()),
    )
    .map_err(|error| AuthError::UnableToEncodeCustomToken { jwt_error: error })
}
