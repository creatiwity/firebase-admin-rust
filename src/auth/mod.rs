extern crate jsonwebtoken;
extern crate pem;
extern crate reqwest;

mod error;
mod service;

use crate::app::App;
pub use error::AuthError;
use service::decode_firebase_token;
pub use service::{DecodedIdToken, SignInProvider};

pub async fn verify_id_token(app: App, id_token: String) -> Result<DecodedIdToken, AuthError> {
    decode_firebase_token(app, id_token).await
}

pub fn create_custom_token(app: App, uid: String) -> Result<String, AuthError> {
    service::create_custom_token(app, uid)
}
