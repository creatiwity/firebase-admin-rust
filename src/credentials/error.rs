use custom_error::custom_error;

custom_error! {pub IdAuthError
    IdTokenExpired = "The provided Firebase ID token is expired.",
    SessionCookieExpired = "The Firebase session cookie has been revoked."
}
