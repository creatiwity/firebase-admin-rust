use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::prelude::*;
use url::Url;

mod error;

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(rename_all = "snake_case")]
pub enum CertificateType {
    ServiceAccount,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Certificate {
    pub r#type: CertificateType,
    pub project_id: String,
    pub private_key_id: String,
    pub private_key: String,
    pub client_email: String,
    pub auth_uri: Url,
    pub token_uri: Url,
    pub auth_provider_x509_cert_url: Url,
    pub client_x509_cert_url: Url,
}

pub fn load_certificate(path: String) -> Certificate {
    let mut file = File::open(path).expect("Firebase configuration file not found.");
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("Unable to read Firebase configuration file.");
    let certificate: Certificate =
        serde_json::from_str(contents.as_str()).expect("Firebase configuration file malformed.");
    return certificate;
}
